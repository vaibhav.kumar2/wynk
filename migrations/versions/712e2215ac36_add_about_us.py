"""Add about us

Revision ID: 712e2215ac36
Revises: 51c820b75e10
Create Date: 2019-09-30 12:58:16.653725

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '712e2215ac36'
down_revision = '51c820b75e10'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('song', sa.Column('about', sa.String(length=2048), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('song', 'about')
    # ### end Alembic commands ###
