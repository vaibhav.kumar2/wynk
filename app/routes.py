from flask import render_template, url_for, redirect, flash, abort
from app import app
from app.forms import LoginForm, SongForm, SearchForm
from flask_login import current_user, login_user
from app.models import User, Song
from flask_login import logout_user, login_required
from flask import request
from werkzeug.urls import url_parse
from app import db
from app.forms import RegistrationForm
import os
from flask_wtf.file import FileField
from werkzeug.utils import secure_filename
from flask import g
from datetime import datetime


@app.route('/')
@app.route('/home', methods = ['GET'])
def home():
    page = request.args.get('page', 1, type=int)
    music = Song.query.all()
    hindi_music = Song.query.filter_by(language='Hindi').paginate(page, app.config['SONGS_PER_PAGE'], False)
    english_music = Song.query.filter_by(language='English').paginate(page, app.config['SONGS_PER_PAGE'], False)

    hindi_next_url = url_for('home', page=hindi_music.next_num) \
        if hindi_music.has_next else None
    hindi_prev_url = url_for('home', page=hindi_music.prev_num) \
        if hindi_music.has_prev else None

    english_next_url = url_for('home', page=english_music.next_num) \
        if english_music.has_next else None
    english_prev_url = url_for('home', page=english_music.prev_num) \
        if english_music.has_prev else None

    g.find_music = SearchForm()
    if g.find_music.validate():
        page = request.args.get('page', 1, type=int)
        song, total = Song.search(g.find_music.q.data, page, 10)
        return render_template('home.html', music=music, hindi_music = hindi_music.items, hindi_next_url=hindi_next_url, hindi_prev_url=hindi_prev_url, english_music=english_music.items, english_next_url=english_next_url, english_prev_url=english_prev_url, find_music=g.find_music, song=song)


    return render_template('home.html', music=music, hindi_music = hindi_music.items, hindi_next_url=hindi_next_url, hindi_prev_url=hindi_prev_url, english_music=english_music.items, english_next_url=english_next_url, english_prev_url=english_prev_url, find_music=g.find_music)


@app.route('/home/artists/<artist>', methods = ['GET'])
def artist_songs(artist):
   songs_of_artist = Song.query.filter_by(artist=artist).all()
   print(songs_of_artist)
   return render_template("artists.html", songs=songs_of_artist, artist=artist)

@app.route('/login', methods = ['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return  redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username = form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for('login'))
        login_user(user, remember = form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('home')
        return redirect(next_page)
    return render_template('login.html', title = 'Sign In', form = form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/admin', methods = ['GET', 'POST'])
@login_required
def admin():
    if current_user.user_type != "admin":
        abort(401)
    form = SongForm()
    if form.validate_on_submit():
        filename = secure_filename(form.file.data.filename)
        form.file.data.save('./app/static/' + filename)
        song_image = secure_filename(form.imagefile.data.filename)
        form.file.data.save('./app/static/' + song_image)
        artist_image = secure_filename(form.artist_image.data.filename)
        form.file.data.save('./app/static/' + artist_image)
        song = Song(name=filename, year=form.year.data, language=form.language.data,\
            artist=form.artist.data, genre=form.genre.data, image= song_image, \
            artist_image=artist_image, about=form.about.data)
        db.session.add(song)
        db.session.commit()
        return redirect(url_for('admin'))
    return render_template('admin.html', form=form)

# @bp.before_app_request
# def before_request():
#     if current_user.is_authenticated:
#         current_user.last_seen = datetime.utcnow()
#         db.session.commit()
#         g.search_form = SearchForm()
#     g.locale = str(get_locale())

# @bp.route('/search')
# @login_required
# def search():
#     if not g.search_form.validate():
#         return redirect(url_for('home'))
#     page = request.args.get('page', 1, type=int)
#     songs, total = Song.search(g.search_form.q.data, page, config['SONGS_PER_PAGE'])
#     next_url = url_for('search', q = g.search_form.q.data, page=page + 1) \
#         if total > page * config['SONGS_PER_PAGE'] else None
#     prev_url = url_for('search', q=g.search_form.q.data, page=page - 1) \
#         if page > 1 else None
#     return render_template('search.html', title=_('Search'), posts=songs,
#                            next_url=next_url, prev_url=prev_url)

