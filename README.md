# Wynk app Clone:
This is a reddit clone project using flask in python.
Flask is a micro framework for python.

# Prequisites: The development for this project requires several packages. Overview is as follows:
* We will use a virtual environment for resolving our project's dependencies.
* Create a virtualenv for your project using "virtualenv -p your/python3/path my_project"
* Activate this virtualenv using "source ~/.virtualenv/venvname/bin/activate" from your root.
* All the libraries installed in this virtual environment are mentioned in the requirements.txt
* Packages/libraries in the virtual environment are installed using pip install pip install package/library name
* Requirements are updated using pip freeze > requirements.txt

# Functionalities of the app:
* Wynk app allows to listen all the Songs that are listed.
* A user can log in.
* A new user can register.
* A logged in user can have access to Artists.
* Admin can have access to add songs and artists.
